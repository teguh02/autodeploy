<?php
/**
 * This's the php pull script from Auto Pull
 * @author Antony Kurniawan <smankursors@stts>
 * @author Benyamin Limanto <me@benyamin.xyz>
 *
 */
echo "\n---------------------\n";
echo "Directory : ".getcwd()."\n";
\chdir("..");
// To folder utama
// @author Anthony Kurniawan <smankursors@stts>
$date = new DateTime();
echo $date->format("Y-m-d H:i:s")."\n";
echo "Directory : ".getcwd()."\n";

// Handle if there are parameters passed
$params = [];

if (isset($argv[1])) {
    foreach ($argv as $key => $value) {
        $tempArr = explode("=", $value);
        if (count($tempArr) > 1) {
            $params[$tempArr[0]] = $tempArr[1];
        }
    }
}

if (isset($params['pre_deployment_commands'])) {
    echo "\nRunning pre deployment commands...\n";

    $escapedCommands = explode(',', $params['pre_deployment_commands']);

    foreach ($escapedCommands as $escapedCommand) {
        $command = str_replace('%20', ' ', $escapedCommand);

        echo "\nRunning \"{$command}\"...\n";
        echo shell_exec($command);
    }
}

echo "\nRunning git pull...\n";
echo shell_exec('git pull');

if (isset($params["submodule"]) && $params["submodule"] == 1) {
    echo "Updating submodule....";
    shell_exec("git submodule foreach --recursive 'git reset --hard'");
    shell_exec("git submodule update --recursive");
}

// Get the HOME path of the running script user!
$home = $_SERVER["HOME"];
// Dump Autoload
putenv("COMPOSER_HOME=$home/.composer");
echo "\nInstall Composer dependencies\n";
echo shell_exec("composer install");
echo "Running migrate...\n";
echo shell_exec("php artisan migrate --force");

if (isset($params['post_deployment_commands'])) {
    echo "\nRunning post deployment commands...\n";

    $escapedCommands = explode(',', $params['post_deployment_commands']);

    foreach ($escapedCommands as $escapedCommand) {
        $command = str_replace('%20', ' ', $escapedCommand);

        echo "\nRunning \"{$command}\"...\n";
        echo shell_exec($command);
    }
}

echo "\n---------------------\n";
