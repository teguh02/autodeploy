<?php 

// @see  https://github.com/bcmw/lumen-skeleton/blob/master/app/Providers/RouteServiceProvider.php
//

namespace Easycode\Autopull\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class AutopullProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/autodeploy.php', 'autodeploy');
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/../../config/autodeploy.php' => config_path('autodeploy.php'),
        ], 'config');

        Route::post('/hook.json',"\Easycode\Autopull\Controllers\WebHookController@handleHook");
        // $this->app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
        //      /*
        //      |--------------------------------------------------------------------------
        //      | Application Routes
        //      |--------------------------------------------------------------------------
        //      |
        //      | Here is where you can register all of the routes for an application.
        //      | It's a breeze. Simply tell Laravel the URIs it should respond to
        //      | and give it the controller to call when that URI is requested.
        //      |
        //      */
        //      $app->post('/hook.json',"\Easycode\Autopull\Controllers\WebHookController@handleHook");
        //  }); 
    }
}
