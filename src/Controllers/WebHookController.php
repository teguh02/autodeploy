<?php

namespace Easycode\Autopull\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class WebHookController extends Controller
{
    public function handleHook(Request $request)
    {
        header('Content-Type: application/json');

        // Check does the webhook send a token? if not then 'jegal!' 
        if (!isset($_SERVER["X-Gitlab-Token"]) && !isset($_SERVER["HTTP_X_GITLAB_TOKEN"])) {
            // API::statusCode(403, "Who are you? You are not allowed to go here!");
            return response()
                ->json(["code" => "403", "message" => "Who are you? You are not allowed to go here!"], 403);
        } else {
            if (isset($_SERVER["HTTP_X_GITLAB_TOKEN"])) {
                $code = $_SERVER["HTTP_X_GITLAB_TOKEN"];
            } else {
                $code = $_SERVER["X-Gitlab-Token"];
            }
            // if ($code != "ez_webhook") {
            if ($code != "ez_webhook") {
                // Logger::handleLog($code);
                Log::error("Code Token Error : " . $code);
                // API::statusCode(403, "Wrong key, check your key again!");
                return response()
                    ->json(["code" => "403", "message" => "Wrong key, check your key again!"], 403);
            }
        }

        // Check does the Gitlab Hook specify the event? If not then don't do it
        if (!isset($_SERVER["X-Gitlab-Event"]) && !isset($_SERVER["HTTP_X_GITLAB_EVENT"])) {
            // API::statusCode(400, "You do a bad request, sorry!");
            return response()->json(["code" => 400, "message" => "You do a bad request, sorry!"], 400);
        } else {
            if (isset($_SERVER["X-Gitlab-Event"])) {
                $event = $_SERVER["X-Gitlab-Event"];
            } else {
                $event = $_SERVER["HTTP_X_GITLAB_EVENT"];
            }
        }

        /**
         * Handling the gitlab event for push and merge request
         * Return error if it's other than two
         * @see https://gitlab.com/help/user/project/integrations/webhooks#merge-request-events
         * @see https://gitlab.com/help/user/project/integrations/webhooks#push-events
         */
        if (\array_search($event, ["Push Hook", "Merge Request Hook"]) === \false) {
            // API::statusCode(406, "Not acceptable, this hook only handle push and merge only!");
            return response()->json(["code" => 406, "message" => "Not acceptable, this hook only handle push and merge only!"])
                ->setStatusCode(406);
        }

        // Get the data from Input as array
        // $data = API::getInputAsArray();
        $data = $request->json()->all();

        // Check if the type is merge
        if ($data["object_kind"] == "merge_request") {
            // Logger::handleLog($data);
            Log::info("Merge : Done");
            return $this->handleMerge($data);
        } else if ($data["object_kind"] == "push") { // Check if type is push
            Log::info("Push : Done");
            return $this->handlePush($data);
        }
    }

    /**
     * Function for handling merge request
     *
     * @param array $data Array from input
     * @return Response
     */
    public function handleMerge(array $data)
    {
        // skip on merge request test
        if (! isset($data['object_attributes']['action']) || ! isset($data['object_attributes']['state'])) {
            return response()->json([
                'code'    => 200,
                'message' => "No action taken, it's not for deployment! Thanks for informing",
            ]);
        }

        // Check does the merge succesful? use env function
        if (
            $data["object_attributes"]["target_branch"] == config("autodeploy.branch_deploy")
            && $data["object_attributes"]["merge_status"] == "can_be_merged"
            && $data["object_attributes"]["action"] == "merge"
            && $data["object_attributes"]["state"] == "merged"
        ) {
            $date = new \DateTime();

            $param = $this->getScriptParam();

            // Do Pull code 
            shell_exec("php " . \realpath(__DIR__ . "/..") . "/scripts/deployment.php $param >> " . base_path() . "/storage/logs/webhook-" . $date->format("Y-m-d") . " 2>&1 &");
            // API::statusCode(202, "Accepted and will be done in minutes");
            return response()->json(["code" => 202, "message" => "Accepted and will be done in minutes"])->setStatusCode(202);
        } else {
            // API::statusCode(200, "No action taken, it's not for deployment! Thanks for informing");
            return response()->json(["code" => 200, "message" => "No action taken, it's not for deployment! Thanks for informing"])
                ->setStatusCode(200);
        }
    }

    public function handlePush(array $data)
    {
        // Use branch deploy index on xample
        if (strpos($data["ref"], config("autodeploy.branch_deploy")) !== false) {
            $date = new \DateTime();

            $param = $this->getScriptParam();

            shell_exec("php " . \realpath(__DIR__ . "/..") . "/scripts/deployment.php $param >> " . base_path() . "/storage/logs/webhook-" . $date->format("Y-m-d") . " 2>&1 &");
            //API::statusCode(202, "Accepted and will be done in minutes");
            return response()->json(["code" => 202, "message" => "Accepted and will be done in minutes"])->setStatusCode(202);
        } else {
            //API::statusCode(200, "No action taken, it's not for deployment! Thanks for informing");
            return response()->json(["code" => 200, "message" => "No action taken, it's not for deployment! Thanks for informing"])
                ->setStatusCode(200);
        }
    }

    private function getScriptParam()
    {
        $params = [];

        $this->handleSubmodule($params);

        $this->handleDeploymentHook($params, 'pre');

        $this->handleDeploymentHook($params, 'post');

        return implode(' ', $params);
    }

    private function handleSubmodule(array &$params)
    {
        if (config("autodeploy.using_submodule") == 1) {
            $params[] = "submodule=1";
        }
    }

    private function handleDeploymentHook(array &$params, string $period)
    {
        $hook = config("autodeploy.hooks.{$period}");

        if (is_array($hook) && isset($hook[App::environment()])) {
            $commands = $hook[App::environment()];

            if (is_array($commands)) {
                $countCommands = count($commands);

                if ($countCommands) {
                    $param = "{$period}_deployment_commands=";

                    foreach ($commands as $index => $command) {
                        $escapedCommand = str_replace(' ', '%20', $command);

                        $param .= $escapedCommand;

                        if (($index + 1) < $countCommands) {
                            $param .= ',';
                        }
                    }

                    $params[] = $param;
                }
            }
        }
    }
}
