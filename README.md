# Auto Deploy EZ 1.0.0

**Author : Benyamin Limanto <ben@easycode.id>**

_Last Update : 30/01/2020_

## About Auto Deploy

AutoDeploy is a composer package for lumen(only for lumen now, because of composer dependencies), for autodeploying on server using gitlab webhook automatically. You only need to install and configure it once, and forgot the rest of deployment. 

## Requirement
- PHP >= 7.2
- Gitlab.com / Gitlab EE with WebHook Support
- Lumen >= 5.8
- Server with SSH key already registered to Gitlab 

## Quick Start

1. Edit the composer.json "repositories" index and add a vcs record like code below :

```json
{
    "name": "laravel/lumen",
    "description": "The Laravel Lumen Framework.",
    "keywords": ["framework", "laravel", "lumen"],
    "license": "MIT",
    "type": "project",
    "require": {
        "php": "^7.2",
        "arcphysx/proofme": ">=1.0",
        "easycode/autopull": "^1.0",
        "flipbox/lumen-generator": "^6.0",
        "laravel/lumen-framework": "^6.0",
        "league/flysystem": "^1.0"
    },
    "require-dev": {
        "barryvdh/laravel-ide-helper": "^2.6",
        "fzaninotto/faker": "^1.4",
        "mockery/mockery": "^1.0",
        "phpunit/phpunit": "^8.0"
    },
    "autoload": {
        "classmap": [
            "database/seeds",
            "database/factories"
        ],
        "psr-4": {
            "App\\": "app/"
        }
    },
    "autoload-dev": {
        "classmap": [
            "tests/"
        ]
    },
    "scripts": {
        "post-root-package-install": [
            "@php -r \"file_exists('.env') || copy('.env.example', '.env');\""
        ]
    },
    "config": {
        "preferred-install": "dist",
        "sort-packages": true,
        "optimize-autoloader": true
    },
    "minimum-stability": "dev",
    "prefer-stable": true,
    "repositories": [
        {
            "type": "vcs",
            "url": "ssh://git@gitlab.com/easycode.id/framework/proofme.git"
        },
        {
            "type": "vcs",
            "url": "ssh://git@gitlab.com/easycode.id/framework/autodeploy.git"
        }
    ]
}
```

2. Run `composer install` on the directory of the lumen project, to install the package. If there's no new package installed, you need to delete the `vendor` folder and the `composer.lock` file and re-run `composer install` command.

3. Configure on Gitlab Webhook on this [WebHook Docs](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) with webhook url `https://{{your_app_url}}/hook.json` and secret token ez_webhook and you must check the push event and merge event checkbook, and fill with master on push event for staging server or production for production server.

4. Configure the lumen .env file by adding this code, for saving keep, add to .env.example file too
```
# The branch that's work for this instance app
BRANCH_DEPLOY=master
# Using Submodule or not 0 for not, 1 for yes
USING_SUBMODULE=0
```

5. Publish the configuration file with command

```shell
php artisan vendor:publish --provider="Easycode\Autopull\Providers\AutopullProvider" --tag=config
```

6. Test the webhook from gitlab, and see if it's working. 
